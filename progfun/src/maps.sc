object maps {
  val capitalOfCountry = Map("US" -> "Washington", "Switzerland" -> "Bern")
                                                  //> capitalOfCountry  : scala.collection.immutable.Map[String,String] = Map(US ->
                                                  //|  Washington, Switzerland -> Bern)
  capitalOfCountry get "us" match {
  	case None => "Missing Data"
  	case Some(capital) => capital
  }                                               //> res0: String = Missing Data
  
  
  ("Hello" groupBy (c => c) map { case (c, cs) => (c, cs.length) }).toList
                                                  //> res1: List[(Char, Int)] = List((e,1), (l,2), (H,1), (o,1))
}