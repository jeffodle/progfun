package forcomp
import forcomp.Anagrams._
object scratch {
  val sentence = List()                           //> sentence  : List[Nothing] = List()
  val sa = sentenceAnagrams(sentence)             //> sa  : List[forcomp.Anagrams.Sentence] = List(List())
  sa == List(Nil)                                 //> res0: Boolean = true

  val sentence2 = List("Linux", "rulez")          //> sentence2  : List[String] = List(Linux, rulez)
  val anas = List(
    List("Rex", "Lin", "Zulu"),
    List("nil", "Zulu", "Rex"),
    List("Rex", "nil", "Zulu"),
    List("Zulu", "Rex", "Lin"),
    List("null", "Uzi", "Rex"),
    List("Rex", "Zulu", "Lin"),
    List("Uzi", "null", "Rex"),
    List("Rex", "null", "Uzi"),
    List("null", "Rex", "Uzi"),
    List("Lin", "Rex", "Zulu"),
    List("nil", "Rex", "Zulu"),
    List("Rex", "Uzi", "null"),
    List("Rex", "Zulu", "nil"),
    List("Zulu", "Rex", "nil"),
    List("Zulu", "Lin", "Rex"),
    List("Lin", "Zulu", "Rex"),
    List("Uzi", "Rex", "null"),
    List("Zulu", "nil", "Rex"),
    List("rulez", "Linux"),
    List("Linux", "rulez"))                       //> anas  : List[List[String]] = List(List(Rex, Lin, Zulu), List(nil, Zulu, Rex)
                                                  //| , List(Rex, nil, Zulu), List(Zulu, Rex, Lin), List(null, Uzi, Rex), List(Rex
                                                  //| , Zulu, Lin), List(Uzi, null, Rex), List(Rex, null, Uzi), List(null, Rex, Uz
                                                  //| i), List(Lin, Rex, Zulu), List(nil, Rex, Zulu), List(Rex, Uzi, null), List(R
                                                  //| ex, Zulu, nil), List(Zulu, Rex, nil), List(Zulu, Lin, Rex), List(Lin, Zulu, 
                                                  //| Rex), List(Uzi, Rex, null), List(Zulu, nil, Rex), List(rulez, Linux), List(L
                                                  //| inux, rulez))

  val sa2 = sentenceAnagrams(sentence2)           //> sa2  : List[forcomp.Anagrams.Sentence] = List(List(Uzi, null, Rex), List(Uzi
                                                  //| , Rex, null), List(Zulu, Lin, Rex), List(Zulu, nil, Rex), List(Zulu, Rex, Li
                                                  //| n), List(Zulu, Rex, nil), List(Lin, Zulu, Rex), List(Lin, Rex, Zulu), List(n
                                                  //| il, Zulu, Rex), List(nil, Rex, Zulu), List(null, Uzi, Rex), List(null, Rex, 
                                                  //| Uzi), List(Linux, rulez), List(rulez, Linux), List(Rex, Uzi, null), List(Rex
                                                  //| , Zulu, Lin), List(Rex, Zulu, nil), List(Rex, Lin, Zulu), List(Rex, nil, Zul
                                                  //| u), List(Rex, null, Uzi))
  sa2.toSet == anas.toSet                         //> res1: Boolean = true

  val so = sentenceOccurrences(sentence2)         //> so  : forcomp.Anagrams.Occurrences = List((e,1), (i,1), (l,2), (n,1), (r,1),
                                                  //|  (u,2), (x,1), (z,1))
  def inner(occurrences: Occurrences): List[Sentence] =
  	if (occurrences.isEmpty) List(Nil)
    else
	    for {
	      o <- combinations(occurrences)
	      w <- dictionaryByOccurrences(o)
	      s <- inner(subtract(occurrences, o))
	    } yield w :: s                        //> inner: (occurrences: forcomp.Anagrams.Occurrences)List[forcomp.Anagrams.Sen
                                                  //| tence]
  
  inner(so)                                       //> res2: List[forcomp.Anagrams.Sentence] = List(List(Uzi, null, Rex), List(Uzi
                                                  //| , Rex, null), List(Zulu, Lin, Rex), List(Zulu, nil, Rex), List(Zulu, Rex, L
                                                  //| in), List(Zulu, Rex, nil), List(Lin, Zulu, Rex), List(Lin, Rex, Zulu), List
                                                  //| (nil, Zulu, Rex), List(nil, Rex, Zulu), List(null, Uzi, Rex), List(null, Re
                                                  //| x, Uzi), List(Linux, rulez), List(rulez, Linux), List(Rex, Uzi, null), List
                                                  //| (Rex, Zulu, Lin), List(Rex, Zulu, nil), List(Rex, Lin, Zulu), List(Rex, nil
                                                  //| , Zulu), List(Rex, null, Uzi))

}