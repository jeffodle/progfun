import patmat.Huffman._

object scratch {
  val plaintext = "the cat is black"              //> plaintext  : String = the cat is black
  val chars = plaintext.toList                    //> chars  : List[Char] = List(t, h, e,  , c, a, t,  , i, s,  , b, l, a, c, k)
  val freqs = times(chars)                        //> freqs  : List[(Char, Int)] = List((t,2), (h,1), (e,1), ( ,3), (c,2), (a,2), 
                                                  //| (i,1), (s,1), (b,1), (l,1), (k,1))
	val leaves = makeOrderedLeafList(freqs)   //> leaves  : List[patmat.Huffman.Leaf] = List(Leaf(h,1), Leaf(e,1), Leaf(i,1), 
                                                  //| Leaf(s,1), Leaf(b,1), Leaf(l,1), Leaf(k,1), Leaf(t,2), Leaf(c,2), Leaf(a,2),
                                                  //|  Leaf( ,3))
  val codeTree = createCodeTree(chars)            //> codeTree  : patmat.Huffman.CodeTree = Fork(Fork(Leaf( ,3),Fork(Leaf(t,2),Lea
                                                  //| f(c,2),List(t, c),4),List( , t, c),7),Fork(Fork(Fork(Leaf(i,1),Leaf(s,1),Lis
                                                  //| t(i, s),2),Fork(Leaf(h,1),Leaf(e,1),List(h, e),2),List(i, s, h, e),4),Fork(L
                                                  //| eaf(a,2),Fork(Leaf(k,1),Fork(Leaf(b,1),Leaf(l,1),List(b, l),2),List(k, b, l)
                                                  //| ,3),List(a, k, b, l),5),List(i, s, h, e, a, k, b, l),9),List( , t, c, i, s, 
                                                  //| h, e, a, k, b, l),16)
	val bits = encode(codeTree)(chars)        //> bits  : List[patmat.Huffman.Bit] = List(0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 1, 0, 
                                                  //| 0, 0, 1, 1, 1, 1, 0, 0, 1, 0, 0, 0, 1, 0, 0, 0, 1, 0, 0, 1, 0, 0, 1, 1, 1, 1
                                                  //| , 0, 1, 1, 1, 1, 1, 1, 1, 0, 0, 1, 1, 1, 1, 1, 0)
}