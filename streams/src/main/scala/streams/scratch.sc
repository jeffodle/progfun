package streams

import common._
import Bloxorz._

object scratch {
	Level0.solution                           //> res0: List[streams.Bloxorz.Level0.Move] = List(Down, Right, Up)
  Level1.solution                                 //> res1: List[streams.Bloxorz.Level1.Move] = List(Right, Right, Down, Right, Ri
                                                  //| ght, Right, Down)
}